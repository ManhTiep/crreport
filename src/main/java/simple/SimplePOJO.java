package simple;

import com.businessobjects.samples.pojo.POJOResultSet;
import com.businessobjects.samples.pojo.POJOResultSetFactory;

import java.sql.Date;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

/*
 * Sample Plain Old Java Object.
 */
public class SimplePOJO {

    public String firstName;
    private String lastName;
    private int id;
    private Date birthDay;


    public SimplePOJO(String firstName, String lastName, int id, Date birthDay) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.id = id;
        this.birthDay = birthDay;
    }

    public String getLastName() {
        return lastName;
    }

    public int getId() {
        return id;
    }

    public Date getBirthDay() {
        return birthDay;
    }

    public ResultSet getAllMonsters() {

        List<SimplePOJO> monsters = new ArrayList<SimplePOJO>();

        monsters.add(new SimplePOJO("Bill", "abc", 30, new Date(99, 11, 31)));


        POJOResultSetFactory factory = new POJOResultSetFactory(SimplePOJO.class);
        POJOResultSet resultSet = factory.createResultSet(monsters);

        return resultSet;

    }
}

